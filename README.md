# jenkins-dockertest

젠킨스에서 도커 명령어 테스트

# 참고자료
* [1] [jenkins공식문서](https://www.jenkins.io/doc/book/pipeline/docker/)
* [2] [블로그] (https://medium.com/faun/docker-build-push-with-declarative-pipeline-in-jenkins-2f12c2e43807)
* [3] [블로그-dockerhub 인증정보관리](https://www.howtodo.cloud/devops/docker/2019/05/16/devops-application.html)